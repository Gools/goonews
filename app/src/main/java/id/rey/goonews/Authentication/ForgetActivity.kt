package id.rey.goonews.Authentication

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import id.rey.goonews.R
import kotlinx.android.synthetic.main.activity_forget.*

class ForgetActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget)

        mAuth = FirebaseAuth.getInstance()
        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setCancelable(false)

        forget.setOnClickListener { ValidationData() }
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }
    fun ValidationData(){
        var mEmail = email_user.text.toString()

        if (mEmail.isEmpty()) {
            email_user.error = "Isi email anda."
            email_user.requestFocus()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            email_user.error = "Isi email anda dengan benar"
            email_user.requestFocus()
        } else {
            forgotPassword(mEmail)
        }
    }

    fun forgotPassword(mEmail: String){
        mProgressDialog!!.setMessage("Sent link verify to: "+mEmail)
        mProgressDialog!!.show()

        mAuth!!.sendPasswordResetEmail(mEmail)
            .addOnCompleteListener { task ->
                mProgressDialog!!.hide()

                if (task.isSuccessful) {
                    val message = "Kami mengirim link ke " + mEmail + "."
                    Log.d("TAG", message)
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ForgetActivity, SigninActivity::class.java))
                    finish()
                } else {
                    Log.w("TAG", task.exception!!.message)
                    Toast.makeText(this,
                        "Tidak ada pengguna ditemukan dengan email ini.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@ForgetActivity, SigninActivity::class.java))
        finish()
    }
}
