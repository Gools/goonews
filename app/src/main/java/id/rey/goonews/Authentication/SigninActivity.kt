package id.rey.goonews.Authentication

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import id.rey.goonews.MainActivity
import id.rey.goonews.MainLayout.NavigationActivity
import id.rey.goonews.R
import id.rey.goonews.Server.GooClient
import id.rey.goonews.Server.GooModel
import kotlinx.android.synthetic.main.activity_signin.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SigninActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mDatabaseReference: DatabaseReference? = null
    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Users")
        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setCancelable(false)

        if (mAuth!!.currentUser != null) {
            startActivity(Intent(this, NavigationActivity::class.java))
            finish()
            return
        }

        signin.setOnClickListener {
            ValidationData()
        }
        signup.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
        forget.setOnClickListener {
            startActivity(Intent(this, ForgetActivity::class.java))
        }
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }

    private fun ValidationData() {
        var mEmail = email_user.text.toString()
        var mPass = password_user.text.toString()

        if (mEmail.isEmpty()) {
            email_user.error = "Isi email anda"
            email_user.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            email_user.error = "Isi email dengan benar"
            email_user.requestFocus()
            return
        }
        if (mPass.isEmpty()) {
            password_user.error = "Isi password anda"
            password_user.requestFocus()
            return
        }
        if (mPass.length < 8) {
            password_user.error = "Password minimal 8 karakter"
            password_user.requestFocus()
            return
        }

        // Kirim data ke fun UserLogin
        UserLogin(mEmail, mPass)
    }

    private fun UserLogin(mEmail: String, mPass: String) {
        mProgressDialog!!.setMessage("Loading . . .") // Mengisi text yang akan ditampilkan pada saat progress
        mProgressDialog!!.show() // Menampilkan Progress dialog pada saat prosses

        // User login
        mAuth!!.signInWithEmailAndPassword(mEmail, mPass)
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    mProgressDialog!!.dismiss()
                    startActivity(Intent(this, NavigationActivity::class.java))
                    finish()
                }
                else {
                    mProgressDialog!!.dismiss()
                    Toast.makeText(this, "Failed Login", Toast.LENGTH_LONG).show()
                }

            })
    }

//    private fun login() {
//        val getLogin = GooClient.get().loginUser(
//            email_user.text.toString(),
//            password_user.text.toString()
//        )
//        getLogin.enqueue(object : Callback<GooModel>{
//
//            override fun onFailure(call: Call<GooModel>, t: Throwable) {
//                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
//            }
//            override fun onResponse(call: Call<GooModel>, response: Response<GooModel>) {
//                Toast.makeText(applicationContext, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                startActivity(Intent(this@SigninActivity, MainActivity::class.java))
//            }
//
//        })
//    }


}
