package id.rey.goonews.Authentication

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import id.rey.goonews.MainLayout.NavigationActivity
import id.rey.goonews.R
import id.rey.goonews.StarterActivity
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {

    // Buat variabel auth & database
    private var mAuth: FirebaseAuth? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mDatabaseReference: DatabaseReference? = null

    // Buat variabel ProgressDialog
    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Users")

        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setCancelable(false)

        signup.setOnClickListener {
//            validation()
            validRegister()
        }
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }

    private fun validRegister() {
        var mUsername = username.text.toString()
        var mEmail = email_user.text.toString()
        var mPass = password_user.text.toString()

        // Periksa apakah username, email, dan pass sudah terisi?
        if (mUsername.isEmpty()){
            username.error = "Isi username anda"
            username.requestFocus()
            return
        }
        if (mEmail.isEmpty()){
            email_user.error = "Isi email anda"
            email_user.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()){
            email_user.error = "Isi email dengan benar"
            email_user.requestFocus()
            return
        }
        if (mPass.isEmpty()){
            password_user.error = "Isi password anda"
            password_user.requestFocus()
            return
        }
        if (mPass.length < 8){
            password_user.error = "Password minimal 8 karakter"
            password_user.requestFocus()
            return
        }

        // Kirim data ke fun RegisterUser
        RegisterUser(mUsername, mEmail, mPass)
    }

    private fun RegisterUser(mUsername: String, mEmail: String, mPass: String) {
        mProgressDialog!!.setMessage("Sign Up")
        mProgressDialog!!.show()

        mAuth!!.createUserWithEmailAndPassword(mEmail, mPass)
            .addOnCompleteListener(OnCompleteListener { task ->

                if (task.isSuccessful){
                    mProgressDialog!!.dismiss()

                    var mUserID = mAuth!!.currentUser!!.uid
                    var mDatabasteReference = mDatabase!!.reference.child("Users")
                    var mDataUser = mDatabasteReference!!.child(mUserID)
                    mDataUser!!.child("Name").setValue(mUsername)

                    verifyEmail()

                    startActivity(Intent(this@SignupActivity, NavigationActivity::class.java))
                    finish()
                }
                else{
                    mProgressDialog!!.dismiss()
                    Toast.makeText(this,"Periksa koneksi / gunakan email lain", Toast.LENGTH_LONG).show()
                }

            })
    }

    private fun verifyEmail() {
        var mUser = mAuth!!.currentUser!!

        mUser!!.sendEmailVerification()
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    Toast.makeText(this@SignupActivity,
                        "We send link verify to: "+mUser!!.email.toString(),
                        Toast.LENGTH_LONG).show()
                }else {
                    Toast.makeText(this@SignupActivity,
                        "Failed to send link verify to: "+mUser!!.email.toString(),
                        Toast.LENGTH_LONG).show()
                }
            }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@SignupActivity, StarterActivity::class.java))
        finish()
    }

//    private fun validation() {
//        val email = email_user.text.toString()
//        val password = password_user.text.toString()
//        val conf_pass = confirm_password.text.toString()
//
//        when {
//            email.isEmpty() -> email_user.error = "Email anda kosong"
//            password.isEmpty() -> password_user.error = "Password anda kosong"
//            else -> {
//                signUp()
//            }
//        }
//    }
//
//    fun signUp() {
//        val data = Bundle()
//
//        data.putString("email", email_user.text.toString())
//        data.putString("password", password_user.text.toString())
//
//        startActivity(Intent(this, SigninActivity::class.java).putExtras(data))
//    }
}
