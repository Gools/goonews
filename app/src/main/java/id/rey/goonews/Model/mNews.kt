package id.rey.goonews.Model

class mNews {
    var Date: String? = null
    var Time: String? = null
    var PostImage: String? = null
    var Description: String? = null

    constructor(): this("","","","") {

    }

    constructor( Date: String?, Time: String?, PostImage: String?, Description: String?) {
        this.Date = Date
        this.Time = Time
        this.PostImage = PostImage
        this.Description = Description
    }
}
