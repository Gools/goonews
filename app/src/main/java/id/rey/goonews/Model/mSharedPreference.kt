package id.rey.goonews.Model

import android.content.Context
import android.content.SharedPreferences

class mSharedPreference(var context: Context) {

    private var preference: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        preference = context.getSharedPreferences("Dummy", 0)
        editor = preference.edit()
    }
    fun setString(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }
    fun getString(key: String): String? {
        return preference.getString(key, "")
    }
}