package id.rey.goonews

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth
import id.rey.goonews.Authentication.SigninActivity
import id.rey.goonews.Authentication.SignupActivity
import id.rey.goonews.MainLayout.NavigationActivity
import kotlinx.android.synthetic.main.activity_starter.*

class StarterActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starter)

//        mAuth = FirebaseAuth.getInstance()

//        if (mAuth!!.currentUser != null) {
//            startActivity(Intent(this, NavigationActivity::class.java))
//            finish()
//            return
//        }

//        button()
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)

        Handler().postDelayed({
            startActivity(Intent(this@StarterActivity, SigninActivity::class.java))
            finish()
        }, 3000)
    }
//    fun button() {
//        signin_button.setOnClickListener {
//            startActivity(Intent(this, SigninActivity::class.java))
//            finish()
//        }
//        signup_button.setOnClickListener {
//            startActivity(Intent(this, SignupActivity::class.java))
//            finish()
//        }
//    }
}

