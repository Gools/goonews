package id.rey.goonews.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.rey.goonews.Test.*

class ViewPagerAdapter (fm : FragmentManager) : FragmentPagerAdapter( fm ) {

    val title = arrayOf("Home", "Notification")

    override fun getItem(position: Int): Fragment {
        when(position) {
            0 -> {
                val fragment = FirstFragment()
                return fragment
            }
            1 -> {
                val fragment1 = SecondFragment()
                return fragment1
            }
            else -> {
                val fragment0 = FirstFragment()
                return fragment0
            }
        }
    }

    override fun getCount(): Int {
        return title.size
    }
}