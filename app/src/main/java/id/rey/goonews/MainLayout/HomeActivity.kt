package id.rey.goonews.MainLayout

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.rey.goonews.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}
