package id.rey.goonews.MainLayout

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.google.firebase.database.*
import id.rey.goonews.R
import kotlinx.android.synthetic.main.activity_detail_news.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class DetailNews : AppCompatActivity() {

    var NewsKey: String? = null
    var newsRef: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news)

        NewsKey = intent.extras.get("NewsKey").toString()
        newsRef = FirebaseDatabase.getInstance().reference.child("Berita").child(NewsKey!!)
        back.setOnClickListener {
            onBackPressed()
            finish()
        }

        newsRef!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {

                detail_desc_news.text = p0.child("Description").value.toString()
                date.text = p0.child("Date").value.toString()
                time.text = p0.child("Time").value.toString()
                detail_desc_news.movementMethod = ScrollingMovementMethod()
                val image = p0.child("PostImage").value.toString()
                Glide.with(this@DetailNews).load(image).into(detail_image_news)
            }
        })
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }
}
