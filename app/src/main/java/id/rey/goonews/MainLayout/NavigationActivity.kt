package id.rey.goonews.MainLayout

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v4.widget.DrawerLayout
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.drawable.DrawerArrowDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import id.rey.goonews.Model.mUser
import id.rey.goonews.Server.GooClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import id.rey.goonews.R
import id.rey.goonews.AdapterUsers
import id.rey.goonews.Authentication.SigninActivity
import id.rey.goonews.Model.mNews
import id.rey.goonews.StarterActivity
import kotlinx.android.synthetic.main.content_navigation.*
import kotlinx.android.synthetic.main.nav_header_navigation.*
import kotlinx.android.synthetic.main.news_item.*
import kotlinx.android.synthetic.main.news_item.view.*


class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var mrecylerview : RecyclerView
    lateinit var ref: DatabaseReference
    var mAuth: FirebaseAuth? = null
    var mDatabase: FirebaseDatabase? = null
    var mDataUser: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        ref = FirebaseDatabase.getInstance().getReference().child("Berita")
        mrecylerview = findViewById<RecyclerView>(R.id.recyclerview_news)
        mrecylerview.layoutManager = LinearLayoutManager(this)
        firebaseData()

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
            startActivity(Intent(this, AddNews::class.java))
        }

        setToolbar()
        initUserData()
//        getUser()
    }

//    class NewsViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView!!){
//        internal var desc: TextView
//    }

    private fun firebaseData() {
        val option = FirebaseRecyclerOptions.Builder<mNews>()
            .setQuery(ref, mNews::class.java)
            .setLifecycleOwner(this)
            .build()

        val firebaseRecyclerAdapter = object: FirebaseRecyclerAdapter<mNews, NewsViewHolder>(option) {


            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
                val itemView = LayoutInflater.from(this@NavigationActivity).inflate(R.layout.news_item,parent,false)
                return NewsViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: NewsViewHolder, position: Int, model: mNews) {



                ref.addValueEventListener(object: ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        Toast.makeText(this@NavigationActivity, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        holder.description.text = model.Description
                        holder.date.text = model.Date
                        holder.time.text = model.Time
//                        Picasso.get().load(model.Image).into(holder.img_vet)
                        Glide.with(this@NavigationActivity).load(model.PostImage).into(holder.postImage)

//                        card_item.setOnClickListener {
//                            val i = Intent(this@NavigationActivity, DetailNews::class.java)
//                            i.putExtra("NewsKey", placeid)
//                            startActivity( i )
//                        }
                        holder.itemView.setOnClickListener {
                            val placeid = getRef(holder.adapterPosition).key
                            val i = Intent(this@NavigationActivity, DetailNews::class.java)
                            i.putExtra("NewsKey", placeid)
                            startActivity( i )
                        }
                    }
                })
            }
        }
        mrecylerview.adapter = firebaseRecyclerAdapter
        firebaseRecyclerAdapter.startListening()
    }
    class NewsViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        internal var description: TextView = itemView!!.findViewById<TextView>(R.id.desc_news)
        internal var date: TextView = itemView!!.findViewById<TextView>(R.id.date)
        internal var time: TextView = itemView!!.findViewById<TextView>(R.id.time)
        internal var postImage: ImageView = itemView!!.findViewById<ImageView>(R.id.image_news)


    }


//    private fun getUser() {
//        val getUsers = GooClient.get().getUsers()
//        getUsers.enqueue(object : Callback<List<mUser>> {
//            override fun onFailure(call: Call<List<mUser>>, t: Throwable) {
//                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onResponse(call: Call<List<mUser>>, response: Response<List<mUser>>) {
//                showData(response.body()!!)
//            }
//        } )
//    }
//
//    private fun showData(users : List<mUser>) {
//        recycler_view.apply {
//            layoutManager = LinearLayoutManager(this@NavigationActivity)
//            adapter = AdapterUsers(users, this@NavigationActivity)
//        }
//    }

    private fun initUserData() {
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()

        val mUser = mAuth!!.currentUser

        val mDatabaseReference = mDatabase!!.reference.child("Users")
        mDataUser = mDatabaseReference.child(mUser!!.uid)

        mDataUser!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(data: DataSnapshot) {
                //data diambil dari database
                username.text = data.child("Name").value.toString()
                //data diambil dari current user
                email_user.text = mUser.email
            }

        })
    }

    private fun setToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Goonews"
        toolbar.setTitleTextColor(Color.WHITE)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        navView.setNavigationItemSelectedListener(this)
        drawerLayout.addDrawerListener(toggle)
        toggle.drawerArrowDrawable.color = Color.WHITE
        toggle.syncState()

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)

    }



    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.navigation, menu)
        return true
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        return when (item.itemId) {
//            R.id.action_settings -> true
//            else -> super.onOptionsItemSelected(item)
//        }
//    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.user -> {
                startActivity(Intent(this, ProfileActivity::class.java))
            }
            R.id.logout -> {
                FirebaseAuth.getInstance().signOut()

                startActivity(Intent(this, SigninActivity::class.java))
                finish()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
