package id.rey.goonews.MainLayout

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import id.rey.goonews.Authentication.SigninActivity
import id.rey.goonews.R
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class ProfileActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    var mDatabase: FirebaseDatabase? = null
    var mDataUser: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initUserData()
        logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            startActivity(Intent(this, SigninActivity::class.java))
            finish()
        }
        back.setOnClickListener {
            onBackPressed()
            finish()
        }
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }

    private fun initUserData() {
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()

        var mUser = mAuth!!.currentUser

        var mDatabaseReference = mDatabase!!.reference.child("Users")
        mDataUser = mDatabaseReference!!.child(mUser!!.uid)

        mDataUser!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(data: DataSnapshot) {
                if (data != null) {

                    //data diambil dari database
                    username.text = data.child("Name").value as String
                    //data diambil dari current user
                    email_user.text = mUser.email
                    username.movementMethod = ScrollingMovementMethod()
                    email_user.movementMethod = ScrollingMovementMethod()

                }
            }

        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@ProfileActivity, NavigationActivity::class.java))
        finish()
    }
}
