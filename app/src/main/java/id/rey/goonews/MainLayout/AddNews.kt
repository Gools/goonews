package id.rey.goonews.MainLayout

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import id.rey.goonews.R
import kotlinx.android.synthetic.main.activity_add_news.*
import java.text.SimpleDateFormat
import java.util.*
import android.R.attr.data
import android.view.WindowManager
import kotlinx.android.synthetic.main.toolbar_layout.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddNews : AppCompatActivity() {

    val GALLERY_PICK = 1
    var resultUri: Uri? = null
    var loadingBar: ProgressDialog? = null

    //Firebase
    private var PostReferenceStorage: StorageReference? = null
    private var userRef: DatabaseReference? = null
    private var postRef: DatabaseReference? = null
    private var mAuth: FirebaseAuth? = null
    private var mDatabase: FirebaseDatabase? = null

    //String
    private var saveCurrentDate: String? = null
    private var saveCurrentTime: String? = null
    private var postRandomName: String? = null
    private var downloadUrl: Uri? = null
    private var idPost: String? = null
    var currentUid: String? = null
    var desc: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_news)

        mDatabase = FirebaseDatabase.getInstance()
        PostReferenceStorage = FirebaseStorage.getInstance().reference
        userRef = FirebaseDatabase.getInstance().reference.child("Users")
        loadingBar = ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance()
        currentUid = mAuth!!.currentUser?.uid

        pick_image.setOnClickListener {
            openGallery()
        }
        upload_news.setOnClickListener {
            validateNews()
        }
        back.setOnClickListener {
            onBackPressed()
            finish()
        }

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = this.getColor(R.color.blue_soft)
    }

    private fun validateNews() {
        desc = desc_news.text.toString()

        if (resultUri == null) {
            Toast.makeText(this, "Select Image First!", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(desc)) {
            Toast.makeText(this, "Fill The Desc!", Toast.LENGTH_SHORT).show()
        } else {
            loadingBar!!.setTitle("Post Berita")
            loadingBar!!.setMessage("Please wait...")
            loadingBar!!.show()
            StoreImage()
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun StoreImage() {
        val calFormDate = Calendar.getInstance()
        val currentDate = SimpleDateFormat("dd-MMMM-yyyy")
        saveCurrentDate = currentDate.format(calFormDate.getTime())

        val calFormTime = Calendar.getInstance()
        val currentTime = SimpleDateFormat("HH:mm")
        saveCurrentTime = currentTime.format(calFormTime.getTime())

        postRandomName = saveCurrentDate + saveCurrentTime

        val filePath = PostReferenceStorage!!.child("newsImage").child(resultUri?.lastPathSegment + postRandomName)
        filePath.putFile(resultUri!!).continueWithTask { task ->
            if (!task.isSuccessful) throw task.exception!!
            return@continueWithTask filePath.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful){
                downloadUrl = task.result!!
                savePostInfo()
                loadingBar!!.dismiss()
                clearField()
                Toast.makeText(this, "Upload Success", Toast.LENGTH_SHORT).show()
            }else{
                val message: String? = task.exception!!.message
                loadingBar!!.dismiss()
                Toast.makeText(this, "Error occured: " + message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun clearField() {

        desc_news.setText("")
        desc_news.setHint("Description")
        image_news.setImageDrawable(null)
    }

    private fun savePostInfo() {
        userRef!!.child(mAuth!!.currentUser!!.uid).addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    val Username: String? = p0.child("Name").value.toString()

                    idPost = currentUid + postRandomName

                    postRef = mDatabase!!.reference.child("Berita")
                    val mData = postRef!!.child(idPost.toString())

                    mData.child("Uid").setValue(currentUid)
                    mData.child("Date").setValue(saveCurrentDate)
                    mData.child("Time").setValue(saveCurrentTime)
                    mData.child("PostImage").setValue(downloadUrl.toString())
                    mData.child("Description").setValue(desc)
                    mData.child("Username").setValue(Username)

                }
            }
        })
    }

    private fun openGallery() {
        val galleryIntent = Intent()
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, GALLERY_PICK)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY_PICK && resultCode == Activity.RESULT_OK && data != null) {
            val ImageUri = data.getData();
//            image_news.setImageURI(ImageUri)
            CropImage.activity(ImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setFixAspectRatio(true)
                .setAspectRatio(4,3)
                .start(this)
        }
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)

            if(resultCode == Activity.RESULT_OK) {
                resultUri = result.uri
                image_news.setImageURI(resultUri)
            }
        }
    }
}