package id.rey.goonews

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import id.rey.goonews.Model.mUser
import kotlinx.android.synthetic.main.user_item.view.*

class AdapterUsers (private val users : List<mUser>, private val mContext: Context):
    RecyclerView.Adapter<AdapterUsers.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.user_item, p0, false)
        return ViewHolder( v )
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(mContext).load("http://192.168.43.18:1000/storage/" + users[position].photoProfile).into(holder.photoProfile)
        holder.username.text = users[position].username
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val photoProfile : ImageView = itemView.user_image
        val username : TextView = itemView.username
    }
}