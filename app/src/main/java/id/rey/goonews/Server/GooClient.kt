package id.rey.goonews.Server

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object GooClient{

    private var apiServices: ApiServices? = null

    private var BASE_URL = "http://192.168.43.18:1000/"
    fun get(): ApiServices {
        if ( apiServices == null ) {
            var retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            apiServices = retrofit.create(ApiServices::class.java)
        }
        return apiServices!!
    }
}