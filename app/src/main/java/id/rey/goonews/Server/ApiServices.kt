package id.rey.goonews.Server

import id.rey.goonews.Model.mUser
import retrofit2.Call
import retrofit2.http.*


interface ApiServices {

    @POST("login")
    @FormUrlEncoded
    fun loginUser(@Field("username") username: String,
                  @Field("password") password: String): Call<GooModel>

    @GET("getUsers")
    fun getUsers(): Call<List<mUser>>



}