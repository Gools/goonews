package id.rey.goonews.Test


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.rey.goonews.R
import id.rey.goonews.Event.mEvent
import kotlinx.android.synthetic.main.fragment_first.view.*
import org.greenrobot.eventbus.EventBus

class FirstFragment : Fragment() {

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_first, container, false)

        init( v )

        return v
    }

    private fun init(view: View) {
        view.button_bus!!.setOnClickListener {
            if( view.text_bus.text.isEmpty()) view.text_bus.error = "Isi Dahulu"
            else {
                val name = view.text_bus.text.toString()
                EventBus.getDefault().post(mEvent.onEventMain(name))
            }
        }
    }


}
