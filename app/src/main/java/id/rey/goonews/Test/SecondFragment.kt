package id.rey.goonews.Test


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rey.goonews.Event.mEvent

import id.rey.goonews.R
import id.rey.goonews.Model.mSharedPreference
import kotlinx.android.synthetic.main.fragment_second.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class SecondFragment : Fragment() {

    private var parentView: View? = null
    private var data: mSharedPreference? = null

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_second, container, false)
        this.parentView = v

        data = mSharedPreference(context!!)
        v.show_text.text = data!!.getString("text")

        return v
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    @Subscribe
    fun textReceived(event: mEvent.onEventMain) {
        this.parentView!!.show_text.text = event.name
        this.data!!.setString("name", event.name)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
    }

}
