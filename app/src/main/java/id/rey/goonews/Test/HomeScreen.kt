package id.rey.goonews.Test

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.util.TypedValue
import id.rey.goonews.R
import id.rey.goonews.Adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.screen_home.*

class HomeScreen : AppCompatActivity() {

    var adapter: ViewPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_home)

        setPager()
    }

    fun setPager() {

        val title = arrayOf("Home", "Notification")

        val iconOff = arrayOf(
            R.drawable.home,
            R.drawable.midd
        )
        val iconOn = arrayOf(
            R.drawable.home_fill,
            R.drawable.midd_fill
        )

        adapter = ViewPagerAdapter(supportFragmentManager)
        main_pager!!.adapter = adapter
        main_pager!!.offscreenPageLimit = 4
        main_tab!!.setupWithViewPager(main_pager)

        for(i in iconOff.indices) {
            main_tab!!.getTabAt(i)!!.icon = applicationContext.getDrawable(iconOff[i])
        }
        main_tab.getTabAt(0)!!.icon = applicationContext.getDrawable( iconOn[0] )

        main_tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
                
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                p0!!.icon = applicationContext.getDrawable(iconOff[p0.position])
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                p0!!.icon = applicationContext.getDrawable(iconOn[p0.position])
            }

        })

//        for( i in title.indices) {
//            main_tab.getTabAt(i)!!.text = title[i]
//        }
    }
}
